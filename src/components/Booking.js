import React, { Component } from 'react';

class Booking extends Component {
    render() {
        return (
            <div className="booking">
                <ul>
                    <li>{this.props.booking.id}</li>
                    <li>{this.props.booking.exportState}</li>
                </ul>
            </div>
        );
    }
}

export default Booking;
