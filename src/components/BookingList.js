import React, { Component } from 'react';
import Booking from './Booking'
import $ from 'jquery';

//https://stackoverflow.com/questions/46190574/how-to-import-signalr-in-react-component
window.jQuery = $;
require('signalr');

class BookingList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            bookingList: []
        };
    }


    componentDidMount() {
        $.get('http://localhost:12950/api/test/bookings', bookings => {
            const bookingList = bookings.map(booking => {
                return {
                    id: booking,
                    exportState: "Unknown"
                }
            });

            this.setState({
                bookingList: bookingList
            });
        });

        const connection = $.hubConnection('http://localhost:12950/signalr');
        const proxy = connection.createHubProxy('testHub');
        proxy.on('onExportStarted', this.onExportStarted.bind(this));
        connection.start()
            .done(function () { console.log('Now connected, connection ID=' + connection.id); })
            .fail(function () { console.log('Could not connect'); });
    }

    onExportStarted(bookingId) {
        this.setState(prevState => {
            const newBookingList = [...prevState.bookingList];
            var existingBookings = $.grep(newBookingList, function (e) { return e.id === bookingId; });
            if (existingBookings.length === 1) {
                existingBookings[0].exportState = "Started";
            } else {
                newBookingList.push({ id: bookingId, exportState: "Unknown" });
            }

            return {
                bookingList: newBookingList
            };
        });
    };

    render() {
        const bookings = this.state.bookingList.map(b => <Booking key={b.id} booking={b} />);

        return (
            <div className="bookingList">
                {bookings}
            </div>
        );
    }
}

export default BookingList;
