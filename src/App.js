import React, { Component } from 'react';
import './App.css';

import BookingList from './components/BookingList';

class App extends Component {
  render() {
    return (
      <div className="App">
        <BookingList />
      </div>
    );
  }
}

export default App;
